# Application of FL task
from MLModel import *
from FLModel import *
from utils import *

from torchvision import datasets, transforms, models
import torch
import numpy as np
import os

# os.environ["CUDA_VISIBLE_DEVICES"] = "2"
# os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
# torch.backends.cudnn.benchmark = True
# device = torch.device("cpu")
# torch.set_default_dtype(torch.float64)

def load_cnn_cifar100(num_users):
    train_d = torchvision.datasets.CIFAR100(root="~/data/", train=True, download=True, transform=transforms.ToTensor())
    valid_d = torchvision.datasets.CIFAR100(root="~/data/", train=False, download=True, transform=transforms.ToTensor())
    # train_d = train.data.float().unsqueeze(1)
    # train_d = train.data.double().unsqueeze(1)
    train_lbls = torch.tensor(train_d.targets,dtype=torch.int64)
    train_data = torch.tensor(train_d.data,dtype=torch.float32)
    train_data = train_data.permute(0,3,1,2)
    train_label= train_lbls

    mean = train_data.mean()
    std = train_data.std()
    train_data = (train_data - mean) / std

    test_data=torch.tensor(valid_d.data,dtype=torch.float32)
    test_data = test_data.permute(0,3,1,2)
    test_lbls = torch.tensor(valid_d.targets,dtype=torch.int64)
    test_label=test_lbls

    testmean = test_data.mean()
    teststd = test_data.std()
    test_data = (test_data - testmean) / teststd

    non_iid = []
    user_dict = mnist_noniid(train_label, num_users)
    for i in range(num_users):
        idx = user_dict[i]
        d = train_data[idx]
        targets = train_label[idx].float()
        non_iid.append((d, targets))
    non_iid.append((test_data.float(), test_label.float()))
        
    return non_iid


"""
1. load_data
2. generate clients (step 3)
3. generate aggregator
4. training
"""
client_num = 20
d = load_cnn_cifar100(client_num)


"""
FL model parameters.
"""
import warnings
warnings.filterwarnings("ignore")

lr = 0.0168
batch_size=256
C=0.5 ##portion of client set to use
q = (batch_size*(client_num*C))/(len(d[1][1])*(client_num))
fl_param = {
    'output_size': 100,
    'client_num': client_num,
    'model': 'resnet18',
    'data': d,
    'lr': lr,
    'E': 1, # number of local iterations
    'C': C,
    'eps': 2,
    'delta': 1e-5,
    'q': q,
    'clip': 1.0,
    'tot_T': 500, #number of aggregation times (communication rounds)
    'batch_size': batch_size,
    'device': device
}

fl_entity = FLServer(fl_param).to(device)


import time

acc = 0
trainacc=0
acc1 = []
trainacc1=[]
start_time = time.time()
for t in range(fl_param['tot_T']):
    acc, trainacc, globalmodel_st,coi_first_comm = fl_entity.global_update()
    if t==0:
        torch.save(coi_first_comm,f'mydata/coi_1stcommround_model.pt')
    #     gradss=[]
    #     st = copy.deepcopy(coi_init_st)
    #     stmodel = copy.deepcopy(coi_model_st)
        
    #     torch.save(coi_init_st,f'mydata/'+'coi_init_model.pt') ###initmodel
    #     torch.save(coi_model_st.state_dict(),f'mydata/'+'coi_1stcommround_model.pt')

    #     with torch.no_grad():
    #         for w_name, w_val in stmodel.named_parameters():
    #             gradss.append((st[w_name] - w_val)/lr)     
        
    #     torch.save({'idx':0,'grads':gradss}, f'mydata/'+'coi_grad.pt')   
    acc1.append(acc)
    trainacc1.append(trainacc)
    print("global epochs = {:d}, train_acc = {:.4f}, test_acc = {:.4f}".format(t+1,trainacc1[-1], acc1[-1]), " Time taken: %.2fs" % (time.time() - start_time))
    if (t+1)==fl_param['tot_T']:
        torch.save(globalmodel_st,'mydata/'+str(t+1)+'coi_modelfinalep_batchnorm_disabled_localt1.pt') ####final comm. round trained model



# SGD (mnt=0.9)


