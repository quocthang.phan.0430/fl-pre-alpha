import matplotlib.pyplot as plt
import torch
import inversefed

setup = inversefed.utils.system_startup()

# dm, ds = torch.tensor(0, **setup), torch.tensor(1, **setup)

# dm = torch.as_tensor(inversefed.consts.cifar100_mean, **setup)[:, None, None]
# ds = torch.as_tensor(inversefed.consts.cifar100_std, **setup)[:, None, None]

dm = torch.as_tensor(inversefed.consts.imagenet_mean, device=torch.device('cpu'))[:, None, None]
ds = torch.as_tensor(inversefed.consts.imagenet_std, device=torch.device('cpu'))[:, None, None]

def plot(tensor):
    tensor = tensor.clone().detach()
    tensor.mul_(ds).add_(dm).clamp_(0, 1)
    if tensor.shape[0] == 1:
        return plt.imshow(tensor[0].permute(1, 2, 0).cpu());
    else:
        fig, axes = plt.subplots(1, tensor.shape[0], figsize=(12, tensor.shape[0]*12))
        for i, im in enumerate(tensor):
            axes[i].imshow(im.permute(1, 2, 0).cpu());
def clamp_image(imgin): ###to [-1,1]
    imgin = imgin.clone().detach().cpu()
    imgin = imgin.mul_(ds).add(dm)
    imgin = ((2 * imgin)/ 1) - 1
    return imgin.detach().cpu().clamp_(-1,1)

def grid_plot(tensor, labels):
    tensor = tensor.clone().detach().cpu()
    tensor.mul_(ds.clone().detach().cpu()).add_(dm.clone().detach().cpu()).clamp_(0, 1)

    fig, axes = plt.subplots(10, 10, figsize=(24, 24))
    for im, l, ax in zip(tensor, labels, axes.flatten()):
        ax.imshow(im.permute(1, 2, 0).cpu());
        ax.set_title(l)
        ax.axis('off')

def print_status(epoch, loss_fn, stats,opts):
    """Print basic console printout every defs.validation epochs."""
    current_lr = opts.param_groups[0]['lr']
    name, format = loss_fn.metric()
    print(f'Epoch: {epoch} | lr: {current_lr:.8f} | '
          f'Train loss is {stats["train_losses"][-1]:6.4f}, Train {name}: {stats["train_" + name][-1]:{format}} | '
          f'Val loss is {stats["valid_losses"][-1]:6.4f}, Val {name}: {stats["valid_" + name][-1]:{format}} |')

def print_status1(epoch, loss_fn, stats,opts):
    """Print basic console printout every defs.validation epochs."""
    current_lr = opts.param_groups[0]['lr']
    name, format = loss_fn.metric()
    print(f'Epoch: {epoch} | lr: {current_lr:.8f} | '
          f'Train loss is {stats["train_losses"][-1]:6.4f}, Train {name}: {stats["train_" + name][-1]:{format}} | ')

