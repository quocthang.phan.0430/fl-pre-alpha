# Application of FL task
from MLModel import *
from FLModel import *
from utils import *

from torchvision import datasets, transforms, models
import torch
import numpy as np
import os

# os.environ["CUDA_VISIBLE_DEVICES"] = "2"
# os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
device = torch.device("cuda:2" if torch.cuda.is_available() else "cpu")
# torch.backends.cudnn.benchmark = True
# device = torch.device("cpu")
# torch.set_default_dtype(torch.float64)

def load_cnn_mnist(num_users):
    train = datasets.MNIST(root="~/data/", train=True, download=True, transform=transforms.ToTensor())
    train_d = train.data.float().unsqueeze(1)
    # train_d = train.data.double().unsqueeze(1)
    train_lbls = train.targets
    train_data = train_d[:50000]
    train_label= train_lbls[:50000]

    mean = train_data.mean()
    std = train_data.std()
    train_data = (train_data - mean) / std

    test_data=train_d[50000:]
    test_label=train_lbls[50000:]
    test_data = (test_data - mean) / std

    # test = datasets.MNIST(root="~/data/", train=False, download=True, transform=transforms.ToTensor())
    # test_data = test.data.float().unsqueeze(1)
    # test_label = test.targets
    # test_data = (test_data - mean) / std

    # split MNIST (training set) into non-iid data sets
    non_iid = []
    user_dict = mnist_noniid(train_label, num_users)
    for i in range(num_users):
        idx = user_dict[i]
        d = train_data[idx]
        targets = train_label[idx].float()
        non_iid.append((d, targets))
    non_iid.append((test_data.float(), test_label.float()))
        
    #     idx = user_dict[i]
    #     d = train_data[idx]
    #     targets = train_label[idx].double()
    #     non_iid.append((d, targets))
    # non_iid.append((test_data.double(), test_label.double()))
    return non_iid


"""
1. load_data
2. generate clients (step 3)
3. generate aggregator
4. training
"""
client_num = 100
d = load_cnn_mnist(client_num)


"""
FL model parameters.
"""
import warnings
warnings.filterwarnings("ignore")

lr = 0.05
batch_size=5
C=1.0 ##portion of client set to use
q = (batch_size*(client_num*C))/(len(d[1][1])*(client_num))
fl_param = {
    'output_size': 10,
    'client_num': client_num,
    'model': MnistCNN_,
    'data': d,
    'lr': lr,
    'E': 100, # number of local iterations
    'C': C,
    'eps': 2,
    'delta': 1e-5,
    'q': q,
    'clip': 4.0,
    'tot_T': 100, #number of aggregation times (communication rounds)
    'batch_size': batch_size,
    'device': device
}

fl_entity = FLServer(fl_param).to(device)


import time

acc = 0
trainacc=0
acc1 = []
trainacc1=[]
start_time = time.time()
for t in range(fl_param['tot_T']):
    acc, trainacc = fl_entity.global_update()
    acc1.append(acc)
    trainacc1.append(trainacc)
    print("global epochs = {:d}, train_acc = {:.4f}, test_acc = {:.4f}".format(t+1,trainacc1[-1], acc1[-1]), " Time taken: %.2fs" % (time.time() - start_time))


# SGD (mnt=0.9)


