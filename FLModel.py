# Federated Learning Model in PyTorch
import torch
import torchvision
import torchvision.transforms as T
from torch import nn
from torch.utils.data import Dataset, DataLoader, TensorDataset
from utils import gaussian_noise
from rdp_analysis import calibrating_sampled_gaussian,search_dp

from MLModel import *

import numpy as np
import copy
from useful_funcs.caleps import _apply_dp_sgd_analysis

class FLClient(nn.Module):
    """ Client of Federated Learning framework.
        1. Receive global model from server
        2. Perform local training (compute gradients)
        3. Return local model (gradients) to server
    """
    def __init__(self, model, output_size, data, lr, E, batch_size, q, clip, sigma, device=None):
        """
        :param model: ML model's training process should be implemented
        :param data: (tuple) dataset, all data in client side is used as training data
        :param lr: learning rate
        :param E: epoch of local update
        """
        super(FLClient, self).__init__()
        self.device = device
        self.BATCH_SIZE = batch_size
        self.torch_dataset = TensorDataset(torch.tensor(data[0]),
                                           torch.tensor(data[1]))
        self.data_size = len(self.torch_dataset)

        # print('data_size: ',self.data_size)

        self.data_loader = DataLoader(
            dataset=self.torch_dataset,
            batch_size=self.BATCH_SIZE,
            shuffle=True,
            num_workers=2,
            pin_memory=True
        )
        self.sigma = sigma    # DP noise level
        self.lr = lr
        self.E = E
        self.clip = clip
        self.q = q

        if model == 'scatter':
            self.model = ScatterLinear(81, (7, 7), input_norm="GroupNorm", num_groups=27).to(self.device)
        if model =='resnet18':
            # self.model = torchvision.models.resnet18(weights=None).to(self.device)
            # self.model = torchvision.models.resnet18(pretrained=True).to(self.device)
            # self.model=resnet18(num_classes=100).to(self.device)
            self.model=resnet18(num_classes=100)
            self.model.to("cpu")
            # pretrainedmodel = torchvision.models.resnet18(pretrained=True)
            # pretrainedmodel.to("cpu")
            # model_dict = self.model.state_dict()
            # pretrained_dict=pretrainedmodel.state_dict()
            # pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in model_dict}
            # model_dict.update(pretrained_dict) 
            
        else:
            self.model = model(data[0].shape[1], output_size).to(self.device)

    def recv(self, model_param):
        """receive global model from aggregator (server)"""
        self.model.to("cpu")
        self.model.load_state_dict(copy.deepcopy(model_param))

    def update(self):
        # torch.autograd.set_detect_anomaly(True)
        """local model update"""
        self.model.to(self.device)
        self.model.train()
        # criterion = nn.CrossEntropyLoss(reduction='none')
        criterion = nn.CrossEntropyLoss()
        optimizer = torch.optim.SGD(self.model.parameters(), lr=self.lr, momentum=0.9)
        # optimizer = torch.optim.Adam(self.model.parameters())
        
        for e in range(self.E):
            # print('local epoch: {:2d}'.format(e))


            optimizer.zero_grad()

            # clipped_grads = {name: torch.zeros_like(param) for name, param in self.model.named_parameters()}
            for batch_x, batch_y in self.data_loader:
                batch_x, batch_y = batch_x.to(self.device,non_blocking=True), batch_y.to(self.device,non_blocking=True)
                pred_y = self.model(batch_x.float())
                # pred_y = self.model(batch_x.double())
                loss = criterion(pred_y, batch_y.long())
                
                # bound l2 sensitivity (gradient clipping)
                # clip each of the gradient in the "Lot"
                # for i in range(loss.size()[0]):
                #     loss[i].backward(retain_graph=True)
                #     torch.nn.utils.clip_grad_norm_(self.model.parameters(), max_norm=self.clip)
                #     for name, param in self.model.named_parameters():
                #         clipped_grads[name] += param.grad/(self.data_size*self.q)
                #         clipped_grads[name] += (1/(self.data_size*self.q))*gaussian_noise(clipped_grads[name].shape, 
                #                                                                             self.clip, self.sigma, device=self.device) 
                loss.backward()
                
            
                # for name, param in self.model.named_parameters():
                #     param.grad = clipped_grads[name]
                
                # update local model
                optimizer.step()
                self.model.zero_grad()


class FLServer(nn.Module):
    """ Server of Federated Learning
        1. Receive model (or gradients) from clients
        2. Aggregate local models (or gradients)
        3. Compute global model, broadcast global model to clients
    """
    def __init__(self, fl_param):
        super(FLServer, self).__init__()
        self.device = fl_param['device']
        self.client_num = fl_param['client_num']
        self.C = fl_param['C']      # (float) C in [0, 1]
        self.clip = fl_param['clip']
        self.T = fl_param['tot_T']  # total number of global iterations (communication rounds)

        self.data = []
        self.target = []
        self.traindata=[]
        self.traintarget=[]
        for sample in fl_param['data'][self.client_num:]:
            self.data += [torch.tensor(sample[0]).to(self.device)]    # test set
            self.target += [torch.tensor(sample[1]).to(self.device)]  # target label
        for sample in fl_param['data'][:self.client_num]:
            self.traindata+=[torch.tensor(sample[0]).to(self.device)]
            self.traintarget += [torch.tensor(sample[1]).to(self.device)]

        self.input_size = int(self.data[0].shape[1])
        self.lr = fl_param['lr']
       
        # compute noise using moments accountant
        # self.sigma = compute_noise(1, fl_param['q'], fl_param['eps'], fl_param['E']*fl_param['tot_T'], fl_param['delta'], 1e-5)
        
        # calibration with subsampeld Gaussian mechanism under composition 
        # self.sigma = calibrating_sampled_gaussian(fl_param['q'], fl_param['eps'], fl_param['delta'], iters=fl_param['E']*fl_param['tot_T'], err=1e-3)
        self.sigma =0.00001
        # self.sigma=0.1
         #1.0 / x.shape[0] * sigma 
        # self.eps=search_dp(fl_param['q'], self.sigma, fl_param['delta'],iters=100)
        print("noise scale = ", self.sigma)
        # print("this source code's epsilon = \n",self.eps)
        orders = ([1.01, 1.0125, 1.025, 1.05, 1.10, 1.25, 1.5, 1.75, 2., 2.25, 2.5, 3., 3.5, 4., 4.5] +
            list(range(5, 64)) + [128, 256, 512])
        _apply_dp_sgd_analysis(
            sample_rate = fl_param['q'],
            noise_multiplier = self.sigma,
            steps= fl_param['E'],
            alphas= orders,
            delta= 1e-05,
            verbose= True)
        
        self.clients = [FLClient(fl_param['model'],
                                 fl_param['output_size'],
                                 fl_param['data'][i],
                                 fl_param['lr'],
                                 fl_param['E'],
                                 fl_param['batch_size'],
                                 fl_param['q'],
                                 fl_param['clip'],
                                 self.sigma,
                                 self.device)
                        for i in range(self.client_num)]

        transform = T.Compose([T.ToTensor(),
                    T.Resize([32, 32],
                    antialias=None)
                    ])
        valset = torchvision.datasets.CIFAR100(root='~/data', train=False, download=True, transform=transform)
        train_loader = torch.utils.data.DataLoader(valset, batch_size=16,
                                            shuffle=False, drop_last=False, 
                                            num_workers=0, 
                                            pin_memory=True)
        x, y = [], []
        idx = 25 # choosen randomly ... just whatever you want
        while len(y) < (16):
            img, label = train_loader.dataset[idx]
            idx += 1
            if label not in y:
                y.append(torch.as_tensor((label,), device=torch.device("cpu")))
                x.append(img.to(torch.device("cpu")))
        imagedata = torch.stack(x)
        labeldata = torch.cat(y)   
        coi_data=[]
        coi_data.append((imagedata.to('cpu'),labeldata.to('cpu')))
               
        self.coi=[FLClient(model=fl_param['model'],
                                 output_size=fl_param['output_size'],
                                 data=coi_data[0],
                                 lr=fl_param['lr'],
                                 E=fl_param['E'],
                                 batch_size=16,
                                 q=fl_param['q'],
                                 clip=fl_param['clip'],
                                 sigma=self.sigma,
                                 device=self.device)]

        torch.save({'idx':0,'ground_truth':imagedata,'labels':labeldata}, f'mydata/'+'coi_dataset.pt')

        if fl_param['model'] == 'scatter':
            self.global_model = ScatterLinear(81, (7, 7), input_norm="GroupNorm", num_groups=27).to(self.device)
        if fl_param['model'] == 'resnet18':
            # self.global_model = torchvision.models.resnet18(weights=None).to(self.device)
            # self.global_model = torchvision.models.resnet18(pretrained=True).to(self.device)
            self.global_model=resnet18(num_classes=100)
            # pretrainedmodel = torchvision.models.resnet18(pretrained=True)
            # model_dict = self.global_model.state_dict()
            # pretrained_dict=pretrainedmodel.state_dict()
            # pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in model_dict}
            # model_dict.update(pretrained_dict) 
            self.global_model.to(self.device)
        else:
            self.global_model = fl_param['model'](self.input_size, fl_param['output_size']).to(self.device)
            
        self.coi_init_state=copy.deepcopy(self.global_model.to(torch.device('cpu')).state_dict())
        torch.save(self.coi_init_state,f'mydata/coi_init_model_localT.pt') ###initmodel
        # self.weight = np.array([client.data_size * 1.0 for client in self.clients])
        self.coi_first_comm=None
        self.broadcast(self.global_model.state_dict())
        
        

    def aggregated(self, idxs_users):
        """FedAvg"""
        model_par = [self.clients[idx].model.state_dict() for idx in idxs_users]
        model_par.append(self.coi[0].model.state_dict())
        new_par = copy.deepcopy(model_par[0])
        for name in new_par:
            new_par[name] = torch.zeros(new_par[name].shape).to(self.device)
        for idx, par in enumerate(model_par):
            # w = self.weight[idxs_users[idx]] / np.sum(self.weight[:])
            # w = self.weight[idx] / np.sum(self.weight[:])
            w=1/(len(idxs_users)+1) ####1 from coi
            for name in new_par:
                # new_par[name] += par[name] * (self.weight[idxs_users[idx]] / np.sum(self.weight[idxs_users]))
                # new_par[name] += par[name] * (w / self.C)
                new_par[name] += par[name] * w
        self.global_model.load_state_dict(copy.deepcopy(new_par))
        return self.global_model.state_dict().copy()

    def broadcast(self, new_par):
        """Send aggregated model to all clients"""
        for client in self.clients:
            client.recv(new_par.copy())
        self.coi[0].recv(new_par.copy())

    def test_acc(self):
        self.global_model.eval()
        correct = 0
        tot_sample = 0
        for i in range(len(self.data)):
            self.xeval_dataset=TensorDataset(self.data[i],self.target[i])
            self.xeval_data_loader = DataLoader(
            dataset=self.xeval_dataset,
            batch_size=1000,
            shuffle=True,
            num_workers=0,
            )
            for iiddxx,(bvalx,bvaly) in enumerate(self.xeval_data_loader):
                bvalx,bvaly=bvalx.to(self.device,non_blocking=True), bvaly.to(self.device,non_blocking=True)
                t_pred_y = self.global_model(bvalx)
                _, predicted = torch.max(t_pred_y, 1)
                correct += (predicted == bvaly).sum().item()
                tot_sample += len(bvaly)
            self.xeval_dataset=None
            self.xeval_data_loader=None
        
        acc = correct / tot_sample
        
        traincorrect=0
        traintot_sample=0
        for i in range(len(self.traindata)):
            t_pred_y = self.global_model(self.traindata[i])
            _, predicted = torch.max(t_pred_y, 1)
            traincorrect += (predicted == self.traintarget[i]).sum().item()
            traintot_sample += self.traintarget[i].size(0)
        trainacc = traincorrect / traintot_sample        
        return acc, trainacc

    def global_update(self):
        # print('torch.backends.cudnn.benchmark: ',torch.backends.cudnn.benchmark)
        # idxs_users = np.random.choice(range(len(self.clients)), int(self.C * len(self.clients)), replace=False)
        idxs_users = np.sort(np.random.choice(range(len(self.clients)), int(self.C * len(self.clients)), replace=False))
        # self.weight = np.array([client.data_size * 1.0 for client in np.array(self.clients)[idxs_users]])
        for idx in idxs_users:
            # print('client#: ',idx)
            self.clients[idx].update()
        self.coi[0].update()
        if self.coi_first_comm is None:
            self.coi_first_comm=copy.deepcopy(self.coi[0].model.state_dict())
        self.broadcast(self.aggregated(idxs_users))
        acc, trainacc = self.test_acc()
        torch.cuda.empty_cache()
        return acc, trainacc, self.global_model.state_dict(), self.coi_first_comm

    def set_lr(self, lr):
        for c in self.clients:
            c.lr = lr

